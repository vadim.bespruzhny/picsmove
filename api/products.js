let products = [
	{
		id: "potato",
		title: "Картошка",
		price: 49.99,
		image: "/products/potato.jpg",
	},
	{
		id: "carrot",
		title: "Морковка",
		price: 55.0,
		image: "/products/carrot.jpg",
	},
	{
		id: "cabbage",
		title: "Капуста",
		price: 28.5,
		image: "/products/cabbage.jpg",
	},
];

const productsApi = {
	getProductsList() {
		return products;
	},
};
export default productsApi;
