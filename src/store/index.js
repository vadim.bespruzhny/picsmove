import api from "../../api/products.js";
import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		cart: [],
		currency: "VGTB",
		products: [],
	},
	mutations: {
		addToCart(state, payload) {
      state.cart.unshift(payload);
		},
    setProducts(state, payload) {
      state.products = payload;
    },
	},
	actions: {
		getProductsList({ commit }) {
      const products = api.getProductsList();
			commit("setProducts", products);
		},
	},
});
